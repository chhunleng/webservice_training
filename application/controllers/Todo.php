<?php
include APPPATH.'libraries/REST_Controller.php';
class Todo extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Todo_model','todo');// load model and alias 
                                                // its name to todo
    }
  	public function getAllTodos_post(){

      // Call to model to get all todos from db
      $data = $this->todo->getAllTodos(); 

      // call to helper
      $response = messsage_success($data);
     
      // response data to client in JSON format
    	$this->response($response); 
  	}

  	public function getTodoById_post(){
  		//echo 'get todo by id';
        // steps to build this service
      // 1. get parameter value of todo id and check require parameter
      $todoId = $this->post('todoId');
      $todoId = (int)$todoId;

      if(is_null($todoId) || empty($todoId)){
        $this->response(messsage_error('missing todoId parameter'));
      }

      // 2. call to model with that id
      $data = $this->todo->getTodoById($todoId);
      
      // 3. response to client 
     if(empty($data)) $data = new stdClass();  // create an object if the data is empty returning from db
     else $data = $data[0];  // access to object the first element in array set

      $response = messsage_success($data);
      $this->response($response);
      

  	}

  	public function addTodo_post(){
  		echo 'add new todo';
  	}

  	public function updateTodo_post(){
  		echo 'update a todo';
  	}

  	public function deleteTodo_post(){
  		echo 'delete a todo';
  	}
}

// $paramId = $this->post('id');

// $data = array(
//  "name" => "Test",
//  "age" => 2,
//  "id" => $paramId
//  // "data" => array(
//  //  "code" => 1,
//  //  "message" => "demo message"
//  // )
// );


?>